#include <cmath>
#include <iostream>
#include <stack>
#include "../thirdParty/la/la.hpp"
using namespace std;
using T=double;


static stack<la::matrix_t<T>> stack_v;
static stack<la::matrix_t<T>> stack_c;

void push(la::matrix_t<T> v) {
  stack_v.push(v);
}
void pop(la::matrix_t<T>& v) {
  v=stack_v.top();
  stack_v.pop();
}

void f_1(la::matrix_t<T>&a,la::matrix_t<T>&B,la::matrix_t<T>&C,la::matrix_t<T>&a_t,la::matrix_t<T>&B_t,la::matrix_t<T>&C_t) {
  #include "./Output/ex3_t.hpp"
}

void f_2(la::matrix_t<T>&a,la::matrix_t<T>&B,la::matrix_t<T>&C,la::matrix_t<T>&B_a,la::matrix_t<T>&C_a) {
  #include "./Output/ex3_a.hpp"
}

int main() {
  int n=1;
  la::matrix_t<T> a=la::matrix_t<T>::Random(n,n);
  la::matrix_t<T> a_t=la::matrix_t<T>::Zero(n,n);
  la::matrix_t<T> B=la::matrix_t<T>::Random(n,n);
  la::matrix_t<T> B_t=la::matrix_t<T>::Ones(n,n);
  la::matrix_t<T> B_a=la::matrix_t<T>::Zero(n,n);
  la::matrix_t<T> C,C_t;
  la::matrix_t<T> C_a=la::matrix_t<T>::Ones(n,n);
  #include "./Input/ex3.hpp"
  cout << C << endl;
  f_1(a,B,C,a_t,B_t,C_t);
  cout << C_t << endl;
  f_2(a,B,C,B_a,C_a);
  cout << B_a << endl;
  return 0;
}
