%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ast.h"

extern int yylex();
extern int yyerror(char*);
extern void lexinit(FILE*); 

static int c=0,cmax=0; //avoid repeated declarations
static char* op; //declare output

void get_memory(YYSTYPE* v) {
  v->c=malloc(BUFFER_SIZE*sizeof(char));
}
void free_memory(YYSTYPE* v) {
  if (v->c) free(v->c); 
}

%}

%token V C L N K I T

%left L
%left K
%left N

%%

code : s
  {
    FILE *fp = NULL;
    fp = fopen(op, "w+");
    for (c=0;c<cmax;c++) fprintf(fp,"la::matrix_t<T> v%d; la::matrix_t<T> v%d_t;\n",c,c);
    fprintf(fp,"%s",$1.c);
    free_memory(&$1);
  }
  ;
s : a
  | a s
  {
    get_memory(&$$);
    sprintf($$.c,"%s%s",$1.c,$2.c);
    free_memory(&$1); free_memory(&$2);
  }
  ;
a : V '=' {c=0;}
  e ';'
  { 
    get_memory(&$$);
    sprintf($$.c,"%s%s_t=v%d_t; %s=v%d;\n", 
                 $4.c,$1.c,$4.j,$1.c,$4.j);
    free_memory(&$1); free_memory(&$4); 
  }
  ;
e : e L e
  {
    $$.j=c++; if (c>cmax) cmax=c;
    get_memory(&$$); 
    sprintf($$.c,"%s%sv%d_t=v%d_t%sv%d_t; v%d=v%d%sv%d;\n",
                 $1.c,$3.c,$$.j,$1.j,$2.c,$3.j,$$.j,$1.j,$2.c,$3.j);
    free_memory(&$1); 
  }
  |e K e
  {
    $$.j=c++; 
    get_memory(&$$); if (c>cmax) cmax=c; 
    sprintf($$.c,"%s%sv%d_t=v%d_t%sv%d_t; v%d=v%d%sv%d;\n",
	         $1.c,$3.c,$$.j,$1.j,$2.c,$3.j,$$.j,$1.j,$2.c,$3.j);
    free_memory(&$1); 
  }
  |e N e
  {
    if (!strcmp($2.c,"*")) {
        $$.j=c++; if (c>cmax) cmax=c;
        get_memory(&$$);
        sprintf($$.c,"%s%sv%d_t=v%d_t*v%d+v%d*v%d_t; v%d=v%d%sv%d;\n",
                     $1.c,$3.c,$$.j,$1.j,$3.j,$1.j,$3.j,$$.j,$1.j,$2.c,$3.j);
        free_memory(&$1); 
    }
  }
  | V '.' I '(' ')'
  {
    $$.j=c++; if (c>cmax) cmax=c;
    get_memory(&$$);
    sprintf($$.c,"v%d_t=%s_t; v%d=%s;\nla::matrix_t<T> _v%d; la::matrix_t<T> _v%d_t;\n_v%d=v%d.inverse();\n_v%d_t=-_v%d*v%d_t*_v%d;\nv%d_t=_v%d_t; v%d=_v%d;\n",
           	 $$.j,$1.c,$$.j,$1.c,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j+1,$$.j,$$.j+1,$$.j);
    cmax++;
    $$.j=c++;    
    free_memory(&$1);
  }
  | V '.' T '(' ')'
  {
    $$.j=c++; if (c>cmax) cmax=c;
    get_memory(&$$);
    sprintf($$.c,"v%d_t=%s_t; v%d=%s;\nla::matrix_t<T> tv%d; la::matrix_t<T> tv%d_t;\ntv%d=v%d.transpose();\ntv%d_t=v%d_t.transpose();\nv%d_t=tv%d_t; v%d=tv%d;\n",
           	 $$.j,$1.c,$$.j,$1.c,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j+1,$$.j,$$.j+1,$$.j);
    cmax++;
    $$.j=c++;    
    free_memory(&$1);
  }  
  | V
  {
    $$.j=c++; if (c>cmax) cmax=c;
    get_memory(&$$);     
    sprintf($$.c,"v%d_t=%s_t; v%d=%s;\n",
     	         $$.j,$1.c,$$.j,$1.c);
  }
  | C
  {
    $$.j=c++; if (c>cmax) cmax=c;
    get_memory(&$$); 
    sprintf($$.c,"v%d_t=%s_t; v%d=%s;\n",
	         $$.j,$1.c,$$.j,$1.c);
    free_memory(&$1); 
  }
;

%%

int yyerror(char *msg) { 
  printf("ERROR: %s \n",msg); 
  return -1; 
}

int main(int argc,char** argv) {
  FILE *source_file=fopen(argv[1],"r");
  lexinit(source_file);
  op=argv[2]; 
  yyparse();
  fclose(source_file);
  return 0;
}

