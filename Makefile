all:
	cd Adjoint && make
	cd Tangent && make
	cd Test && make
clean:
	cd Adjoint && make clean
	cd Tangent && make clean
	cd Test && make clean
	cd Test/Output && make clean
.PHONY: all clean
