#include <cmath>
#include <iostream>
#include <stack>
#include "../thirdParty/la/la.hpp"
using namespace std;
using T=double;


static stack<la::matrix_t<T>> stack_v;
static stack<la::matrix_t<T>> stack_c;

void push(la::matrix_t<T> v) {
  stack_v.push(v);
}
void pop(la::matrix_t<T>& v) {
  v=stack_v.top();
  stack_v.pop();
}

void f_1(la::matrix_t<T>&A,la::matrix_t<T>&B,la::matrix_t<T>&C,la::matrix_t<T>&A_t,la::matrix_t<T>&B_t,la::matrix_t<T>&C_t) {
  #include "./Output/ex2_t.hpp"
}

void f_2(la::matrix_t<T>&A,la::matrix_t<T>&B,la::matrix_t<T>&C,la::matrix_t<T>&A_a,la::matrix_t<T>&B_a,la::matrix_t<T>&C_a) {
  #include "./Output/ex2_a.hpp"
}

int main() {
  int n=1;
  la::matrix_t<T> A=la::matrix_t<T>::Random(n,n);
  la::matrix_t<T> A_t=la::matrix_t<T>::Ones(n,n);
  la::matrix_t<T> A_a=la::matrix_t<T>::Zero(n,n);
  la::matrix_t<T> B=la::matrix_t<T>::Random(n,n), B_in=B;
  la::matrix_t<T> B_t=la::matrix_t<T>::Ones(n,n);
  la::matrix_t<T> B_a=la::matrix_t<T>::Ones(n,n);
  la::matrix_t<T> C,C_t;
  la::matrix_t<T> C_a=la::matrix_t<T>::Zero(n,n);
  #include "./Input/ex2.hpp"
  cout << B << endl;
  B=B_in;
  f_1(A,B,C,A_t,B_t,C_t);
  B=B_in;
  cout << B_t << endl;
  f_2(A,B,C,A_a,B_a,C_a);
  cout << A_a << endl;
  cout << B_a << endl;
  return 0;
}
