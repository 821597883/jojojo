#include <cmath>
#include <iostream>
#include <stack>
#include "../thirdParty/la/la.hpp"
using namespace std;
using T=double;

static stack<la::matrix_t<T>> stack_v;
static stack<la::matrix_t<T>> stack_c;

void push(la::matrix_t<T> v) {
  stack_v.push(v);
}
void pop(la::matrix_t<T>& v) {
  v=stack_v.top();
  stack_v.pop();
}

void f_1(la::matrix_t<T>&A,la::matrix_t<T>&B,la::matrix_t<T>&C,la::matrix_t<T>&D,la::matrix_t<T>&E,la::matrix_t<T>&F,la::matrix_t<T>&A_t,la::matrix_t<T>&B_t,la::matrix_t<T>&C_t,la::matrix_t<T>&D_t,la::matrix_t<T>&E_t,la::matrix_t<T>&F_t) {
  #include "./Output/ex5_t.hpp"
} 

int main() {
  int n=2,m=4;
  la::matrix_t<T> A=la::matrix_t<T>::Random(m,n);
  la::matrix_t<T> A_t=la::matrix_t<T>::Ones(m,n);
  la::matrix_t<T> A_a=la::matrix_t<T>::Zero(m,n);
  la::matrix_t<T> B=la::matrix_t<T>::Random(n,1);
  la::matrix_t<T> B_t=la::matrix_t<T>::Ones(n,1);
  la::matrix_t<T> B_a=la::matrix_t<T>::Zero(n,1);
  la::matrix_t<T> C,C_t;
  la::matrix_t<T> C_a=la::matrix_t<T>::Zero(m,1);
  la::matrix_t<T> D,D_t;
  la::matrix_t<T> D_a=la::matrix_t<T>::Zero(m,1);
  la::matrix_t<T> E=la::matrix_t<T>::Random(m,1);
  la::matrix_t<T> E_t=la::matrix_t<T>::Ones(m,1);
  la::matrix_t<T> E_a=la::matrix_t<T>::Zero(m,1);
  la::matrix_t<T> F,F_t;
  la::matrix_t<T> F_a=la::matrix_t<T>::Zero(1,1);
  #include "./Input/ex5.hpp"
  cout << F << endl;
  f_1(A,B,C,D,E,F,A_t,B_t,C_t,D_t,E_t,F_t);
  cout << F_t << endl;
  return 0;
}
