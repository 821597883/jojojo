%{ 
#include "ast.h"
#include "parser.tab.h"

#include<stdlib.h>
#include<string.h>

void to_parser() {
  yylval.c=(char*)malloc(BUFFER_SIZE*sizeof(char));
  strcpy(yylval.c,yytext);
}
%}

whitespace      [ \t\n]+
variable        [A-Z]
constant        [a-z]

%% 

{whitespace}    { }
"+"             { to_parser(); return L; }
"-"             { to_parser(); return K; }
"*"             { to_parser(); return N; }
"inverse"       { to_parser(); return I; }
"transpose"     { to_parser(); return T; }
{variable}      { to_parser(); return V; }
{constant}      { to_parser(); return C; }
. 		{ return yytext[0]; }

%%

void lexinit(FILE *source) { yyin=source; }
