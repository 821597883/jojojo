// info@stce.rwth-aachen.de

#pragma once

#include "../eigen-3.4.0/Eigen/Dense"

namespace la {

  template<typename T, int N=Eigen::Dynamic>
  using vector_t=Eigen::Matrix<T,N,1>;

  template<typename T, int M=Eigen::Dynamic, int N=M>
  using matrix_t=Eigen::Matrix<T,M,N>;

}
