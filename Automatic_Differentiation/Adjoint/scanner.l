%{ 
#include "ast.h"
#include "parser.tab.h"
%}

whitespace      [ \t\n]+
variable        [A-Z]
constant        [a-z]

%%

{whitespace} { }
"+"          { return L; }
"-"          { return K; }
"*"          { return N; }
"inverse"    { return I; } 
"transpose"  { return T; }
{variable} {
  yylval.af = (char*)malloc(2*sizeof(char));
  strcpy(yylval.af,yytext);
  yylval.ar=0; yylval.j=0;
  return V;			
}
{constant} {
  yylval.af = (char*)malloc((strlen(yytext)+1)*sizeof(char));
  strcpy(yylval.af,yytext);
  yylval.ar=0; yylval.j=0;
  return C;
}
. { return yytext[0]; }

%%

void lexinit(FILE *source) { yyin=source; }
