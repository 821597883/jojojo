%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"

extern int yylex();
extern int yyerror(char*);
extern void lexinit(FILE*);

static int c=0,cmax=0; //avoid repeated declarations
static int mode=0; //rotate different algorithms in multiplication
static char* op; //declare output
%}

%token V C L N K I T

%left L
%left K
%left N 

%%

code : s
  {
    FILE *fp = NULL;
    fp = fopen(op, "w+");
    for (c=0;c<cmax;c++) fprintf(fp,"la::matrix_t<T> v%d; la::matrix_t<T> v%d_a;\n",c,c);
    fprintf(fp,"%s%s",$1.af,$1.ar);
    fclose(fp);
    free($1.af); free($1.ar);
  }

  ;
s : a { $$=$1; }
  | a s
  {
    $$.af=(char*)malloc((strlen($1.af)+strlen($2.af)+1)*sizeof(char)); 
    sprintf($$.af,"%s%s",$1.af,$2.af);
    free($2.af); free($1.af);
    
    $$.ar=(char*)malloc((strlen($1.ar)+strlen($2.ar)+1)*sizeof(char));
    sprintf($$.ar,"%s%s",$2.ar,$1.ar);
    free($2.ar); free($1.ar);  
  }
  ;
a : V '=' e ';'
  {
    $$.af=(char*)malloc((strlen($3.af)+2*strlen($1.af)+$3.j%10+200)*sizeof(char));
    sprintf($$.af,"%spush(%s); %s=v%d;\n",$3.af,$1.af,$1.af,$3.j);
    $$.ar=(char*)malloc((3*strlen($1.af)+$3.j%10+strlen($3.ar)+200)*sizeof(char));
    sprintf($$.ar,"pop(%s); v%d_a=%s_a; %s_a=la::matrix_t<T>::Zero(%s_a.rows(),%s_a.cols());\n%s",$1.af,$3.j,$1.af,$1.af,$1.af,$1.af,$3.ar);
    free($3.ar); free($1.af); free($3.af);
    c=0;
  }
  ;
e : e L e
  {
    $$.af=(char*)malloc((strlen($1.af)+strlen($3.af)+2*c%10+$1.j%10+$3.j%10+21)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;
    sprintf($$.af,"%s%spush(v%d); v%d=v%d+v%d;\n",$1.af,$3.af,$$.j,$$.j,$1.j,$3.j);
    free($1.af); free($3.af);

    $$.ar=(char*)malloc(($1.j%10+$3.j%10+3*$$.j%10+strlen($1.ar)+strlen($3.ar)+28)*sizeof(char));
    sprintf($$.ar,"pop(v%d); v%d_a=v%d_a; v%d_a=v%d_a;\n%s%s",$$.j,$1.j,$$.j,$3.j,$$.j,$3.ar,$1.ar);
    free($1.ar); free($3.ar);
  }
  | e K e
  {
    $$.af=(char*)malloc((strlen($1.af)+strlen($3.af)+2*c%10+$1.j%10+$3.j%10+21)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;
    sprintf($$.af,"%s%spush(v%d); v%d=v%d-v%d;\n",$1.af,$3.af,$$.j,$$.j,$1.j,$3.j);
    free($1.af); free($3.af);

    $$.ar=(char*)malloc(($1.j%10+$3.j%10+3*$$.j%10+strlen($1.ar)+strlen($3.ar)+28)*sizeof(char));
    sprintf($$.ar,"pop(v%d); v%d_a=v%d_a; v%d_a=-v%d_a;\n%s%s",$$.j,$1.j,$$.j,$3.j,$$.j,$3.ar,$1.ar);
    free($1.ar); free($3.ar);
  }
  | e N e
  {
    $$.af=(char*)malloc((strlen($1.af)+strlen($3.af)+2*c%10+$1.j%10+$3.j%10+50)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;
    sprintf($$.af,"%s%spush(v%d); v%d=v%d*v%d;\n",$1.af,$3.af,$$.j,$$.j,$1.j,$3.j);
    free($1.af); free($3.af);
    if (mode==0) {
        $$.ar=(char*)malloc((2*$1.j%10+2*$3.j%10+3*$$.j%10+strlen($1.ar)+strlen($3.ar)+50)*sizeof(char));
        sprintf($$.ar,"pop(v%d); v%d_a=v%d.transpose()*v%d_a; v%d_a=v%d_a*v%d.transpose();\n%s%s",$$.j,$1.j,$1.j,$$.j,$3.j,$$.j,$3.j,$3.ar,$1.ar);
    } else if(mode==1) {    
        $$.ar=(char*)malloc((2*$1.j%10+2*$3.j%10+3*$$.j%10+strlen($1.ar)+strlen($3.ar)+50)*sizeof(char));
        sprintf($$.ar,"pop(v%d); _v%d_a=v%d_a*v%d.transpose(); v%d_a=_v%d.transpose()*v%d_a;\n%s%s",$$.j,$1.j-1,$$.j,$3.j,$3.j,$1.j-1,$$.j,$3.ar,$1.ar);    
    } else if(mode==2){
        $$.ar=(char*)malloc((2*$1.j%10+2*$3.j%10+3*$$.j%10+strlen($1.ar)+strlen($3.ar)+50)*sizeof(char));
        sprintf($$.ar,"pop(v%d); tv%d_a=v%d_a*v%d.transpose(); v%d_a=tv%d.transpose()*v%d_a;\n%s%s",$$.j,$1.j-1,$$.j,$3.j,$3.j,$1.j-1,$$.j,$3.ar,$1.ar);    
    } else if(mode==3){
        $$.ar=(char*)malloc((2*$1.j%10+2*$3.j%10+3*$$.j%10+strlen($1.ar)+strlen($3.ar)+50)*sizeof(char));
        sprintf($$.ar,"pop(v%d); v%d_a=v%d*v%d_a;\n%s%s",$$.j,$3.j,$1.j,$$.j,$3.ar,$1.ar);    
    }
    free($1.ar); free($3.ar);
  }
  | V '.' I '(' ')'
  {
    $$.af=(char*)malloc((2*c%10+strlen($1.af)+120)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;  
    sprintf($$.af,"push(v%d); v%d=%s;\npush(v%d); la::matrix_t<T> _v%d=%s.inverse(); la::matrix_t<T> _v%d_a=la::matrix_t<T>::Zero(_v%d.rows(),_v%d.cols()); v%d=_v%d;\n",$$.j,$$.j,$1.af,$$.j+1,$$.j,$1.af,$$.j,$$.j,$$.j,$$.j+1,$$.j); 

    $$.ar=(char*)malloc((strlen($1.af)+2*$$.j%10+120)*sizeof(char));
    sprintf($$.ar,"pop(v%d); v%d_a=-_v%d.transpose()*_v%d_a*_v%d.transpose(); _v%d_a=la::matrix_t<T>::Zero(_v%d.rows(),_v%d.cols());\npop(v%d); %s_a+=v%d_a;\n",$$.j+1,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$$.j,$1.af,$$.j);
    free($1.af);
    cmax++;
    $$.j=c++;
    mode=1;
  }
  | V '.' T '(' ')'
  {
    $$.af=(char*)malloc((2*c%10+strlen($1.af)+140)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;  
    sprintf($$.af,"push(v%d); v%d=%s;\npush(v%d); la::matrix_t<T> tv%d=%s.transpose(); la::matrix_t<T> tv%d_a=la::matrix_t<T>::Zero(tv%d.rows(),tv%d.cols()); v%d=tv%d;  \n",$$.j,$$.j,$1.af,$$.j+1,$$.j,$1.af,$$.j,$$.j,$$.j,$$.j+1,$$.j); 

    $$.ar=(char*)malloc((strlen($1.af)+2*$$.j%10+120)*sizeof(char));
    sprintf($$.ar,"pop(v%d); v%d_a=tv0_a.transpose(); tv%d_a=la::matrix_t<T>::Zero(tv%d.rows(),tv%d.cols());\npop(v%d); %s_a+=v%d_a;\n",$$.j+1,$$.j,$$.j,$$.j,$$.j,$$.j,$1.af,$$.j);
    free($1.af);
    cmax++;
    $$.j=c++;
    mode=2;
  }
  | V 
  {
    $$.af=(char*)malloc((2*c%10+strlen($1.af)+16)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;
    sprintf($$.af,"push(v%d); v%d=%s;\n",$$.j,$$.j,$1.af);

    $$.ar=(char*)malloc((strlen($1.af)+2*$$.j%10+18)*sizeof(char));
    sprintf($$.ar,"pop(v%d); %s_a+=v%d_a;\n",$$.j,$1.af,$$.j);
    free($1.af);
  }
  | C
  {
    $$.af=(char*)malloc((2*c%10+strlen($1.af)+16)*sizeof(char));
    $$.j=c++; if (c>cmax) cmax=c;
    sprintf($$.af,"push(v%d); v%d=%s;\n",$$.j,$$.j,$1.af);

    $$.ar=(char*)malloc((2*$$.j%10+20)*sizeof(char));
    sprintf($$.ar,"pop(v%d);     ",$$.j);
    free($1.af);
    mode=3;
  }
  ;

%%

int yyerror(char *msg) { printf("ERROR: %s \n",msg); return -1; }

int main(int argc,char** argv)
{
  FILE *source_file=fopen(argv[1],"r");
  lexinit(source_file); 
  op=argv[2];
  yyparse();
  fclose(source_file);
  return 0;
}

